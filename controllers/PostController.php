<?php

namespace app\controllers;

use app\models\Post;
use app\models\PublishForm;
use Yii;
use yii\data\Pagination;
use yii\web\Controller;
use yii\web\UploadedFile;

class PostController extends Controller
{

    public function actionIndex()
    {
        $query = Post::find()->with('user');
        $pages = new Pagination(['totalCount' => $query->count(), 'pageSize' => 4, 'forcePageParam' => false, 'pageSizeParam' => false]);
        $posts = $query->offset($pages->offset)->limit($pages->limit)->all();
        return $this->render('index', compact('posts', 'pages'));
    }

    public function actionView($id)
    {
        $query = Post::find()->where(['id' => $id]);
        $pages = new Pagination(['totalCount' => $query->count(), 'pageSize' => 4, 'forcePageParam' => false, 'pageSizeParam' => false]);
        $posts = $query->offset($pages->offset)->limit($pages->limit)->all();
        return $this->render('post', compact('posts'));
    }

    public function actionUpdate($id)
    {
        $model = Post::findOne($id);
        if ($model->load(Yii::$app->request->post())) {
            $model->save();
            return $this->redirect('index');
        }
        return $this->render('edit', ['model' => $model]);
    }

    public function actionPost()
    {
        $model = new PublishForm();
        if ($model->load(Yii::$app->request->post())) {
            $model->img = UploadedFile::getInstance($model, 'img');
            $way = $model->upload();
            if ($post = $model->publish($way)) {
                return $this->redirect('index');
            }
        }
        return $this->render('publish', ['model' => $model]);
    }

}