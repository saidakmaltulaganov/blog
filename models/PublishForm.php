<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property-read User|null $user
 *
 */
class PublishForm extends Model
{
    public $title;
    public $content;
    public $img;
    public $way;
    public $excerpt;
    public $created_at;
    public $user_id;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['title', 'content', 'img'], 'required'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'title' => 'Title',
            'content' => 'Content',
//            'excerpt' => 'Excerpt',
            'img' => 'Image',
        ];
    }

    public function upload()
    {
        if ($this->validate()) {
            $this->img->saveAs('img/' . $this->img->baseName . '.' . $this->img->extension);
            $this->way = 'img/' . $this->img->baseName . '.' . $this->img->extension;
            return ($this->way);
//            return true;
        } else {
            return false;
        }
    }

    public function publish($way)
    {
        if (!$this->validate())
        {
            return null;
        }
        $post = new Post();
        $this->excerpt = mb_strcut($this->content, 0, 200);
        $post->user_id = Yii::$app->user->getId();
        $post->title = $this->title;
        $post->excerpt = $this->excerpt;
        $post->content = $this->content;
        $post->img = $way;
        $post->created_at = Yii::$app->formatter->asDate('now', 'php:d.m.Y');

        return $post->save() ? $post : null;
    }

}
