<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property-read User|null $user
 *
 */
class RegisterForm extends Model
{
    public $username;
    public $password;
    public $first_name;
    public $last_name;
    public $email;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['username', 'password', 'first_name', 'last_name', 'email'], 'required'],
            ['username', 'unique', 'targetClass' => '\app\models\User', 'message' => 'Такой пользователь уже есть в системе'],
            ['email', 'unique', 'targetClass' => '\app\models\User', 'message' => 'Такой email уже есть в системе'],
            ['email', 'email'],
            ['password', 'string', 'min' => 6],
        ];
    }

    public function attributeLabels()
    {
        return [
            'username' => 'Username',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'email' => 'Email',
            'password' => 'Password',
        ];
    }

    public function register()
    {
        if (!$this->validate())
        {
            return null;
        }
        $user = new User();
        $user->first_name = $this->first_name;
        $user->last_name = $this->last_name;
        $user->username = $this->username;
        $user->email = $this->email;
        $user->HashPassword($this->password);

        return $user->save() ? $user : null;
    }

}
