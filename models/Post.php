<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * @property int id
 * @property int user_id
 * @property string title
 * @property string excerpt
 * @property string content
 * @property string img
 * @property string created_at
 */

class Post extends ActiveRecord
{
//    public $img;
    public static function tableName()
    {
        return 'posts';
    }

    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['title', 'content', 'img'], 'required'],
        ];
    }

}