<?php

//global $model;

use yii\widgets\DetailView;

echo DetailView::widget([
    'model' => $model,
    'attributes' => [
        'id',
        'user_id',
        'title',
        'content',
        'img',
        'created_at'
    ],
]);