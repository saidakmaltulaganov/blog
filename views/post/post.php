<?php

/** @var yii\web\View $this */

$this->title = Yii::$app->name;
?>

<section class="bg-text-area">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="bg-text">
                    <h3>Classic BLOG Design</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut
                        labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
                        laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in
                        voluptate velit esse cillum dolore eu fugiat nulla </p>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="blog-post-area">
    <div class="container">
        <div class="row">
            <div class="blog-post-area-style">
                <?php foreach ($posts as $post): ?>
                    <div class="col-md-3">
                        <div class="single-post">
                            <?= \yii\helpers\Html::img("@web/{$post->img}") ?>
                            <h3>
                                <a href="<?= \yii\helpers\Url::to(['post/view', 'id' => $post->id]) ?>"><?= $post->title ?></a>
                            </h3>
                            <h4><span>Posted By: <span class="author-name">
                                    <a href="<?= \yii\helpers\Url::to(['user/view', 'first_name' => $post->user->first_name]) ?>">
                                        <?= $post->user->first_name ?> <?= $post->user->last_name ?>
                                    </a>
                                </span></span>
                            </h4>
                            <p>
                                <?= $post->content ?>
                            </p>
                            <h4><span><?= Yii::$app->formatter->asDate($post->created_at, 'php:d.m.Y') ?></span></h4>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</section>
