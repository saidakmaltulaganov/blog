<?php
//
///** @var yii\web\View $this */
///** @var yii\bootstrap5\ActiveForm $form */
//
///** @var app\models\PublishForm $model */
//
//use yii\bootstrap5\ActiveForm;
//use yii\bootstrap5\Html;
//
//$this->title = 'Publish';
//$this->params['breadcrumbs'][] = $this->title;
//?>
<!--<div class="site-login">-->
<!--    <h1>--><?php //= Html::encode($this->title) ?><!--</h1>-->
<!--    <p>Please fill out the following fields to publish:</p>-->
<!--    <div class="row">-->
<!--        <div class="col-lg-5">-->
<!--            --><?php //$form = ActiveForm::begin([
//                'id' => 'login-form',
//                'fieldConfig' => [
//                    'template' => "{label}\n{input}\n{error}",
//                    'labelOptions' => ['class' => 'col-lg-1 col-form-label mr-lg-3'],
//                    'inputOptions' => ['class' => 'col-lg-3 form-control'],
//                    'errorOptions' => ['class' => 'col-lg-7 invalid-feedback'],
//                ],
//            ]); ?>
<!---->
<!--            --><?php //= $form->field($model, 'title')->textInput(['autofocus' => true]) ?>
<!--            --><?php //= $form->field($model, 'content')->textarea(['rows' => 5]) ?>
<!--            --><?php //= $form->field($model, 'img') ?>
<!--            <div class="form-group">-->
<!--                <div class="offset-lg-1 col-lg-11">-->
<!--                    --><?php //= Html::submitButton('Publish', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
<!--                </div>-->
<!--            </div>-->
<!---->
<!--            --><?php //ActiveForm::end(); ?>
<!--        </div>-->
<!--    </div>-->
<!--</div>-->


<?php

use yii\bootstrap5\ActiveForm;
use yii\helpers\Html;

$form = ActiveForm::begin(['layout' => 'horizontal']); ?>
<?php //$form = ActiveForm::begin([
//    'id' => 'login-form',
//    'options' => ['class' => 'form-vertical'],
//]); ?>
<?= $form->field($model, 'title') ?>
<?= $form->field($model, 'content')->textarea(['rows' => 9]) ?>
<?php //= $form->field($model, 'excerpt')->textarea(['rows' => 9]) ?>
<?= $form->field($model, 'img')->fileInput() ?>
<div class="form-group">
    <div class="offset-lg-5 col-lg-7">
        <?= Html::submitButton('Publish', ['class' => 'btn btn-primary']) ?>
    </div>
</div>
<?php ActiveForm::end(); ?>
<!--</div>-->
<!--</div>-->