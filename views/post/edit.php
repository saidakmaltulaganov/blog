<?php

use yii\bootstrap5\ActiveForm;
use yii\helpers\Html;

$form = ActiveForm::begin(['layout' => 'horizontal']); ?>
<?= $form->field($model, 'title') ?>
<?= $form->field($model, 'content')->textarea(['rows' => 5]) ?>
<?= $form->field($model, 'img') ?>
    <div class="form-group">
        <div class="offset-lg-5 col-lg-7">
            <?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>