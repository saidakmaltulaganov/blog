<?php

use app\assets\AppAsset;
use app\widgets\Alert;
use yii\bootstrap5\Nav;
use yii\bootstrap5\NavBar;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;


AppAsset::register($this);
?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <base href="/">
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php $this->registerCsrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>

    <body>
    <?php $this->beginBody() ?>
    <div class="wrapper">
        <header class="header">
            <div class="container">
                <div class="row">
                    <div class="col-md-2">
                        <div class="logo">
                            <h2><?php
                                echo Nav::widget([
                                    'items' => [
                                        ['label' => 'My blog', 'url' => ['/post/index']]
                                    ]
                                ]);
                                ?></h2>
                        </div>
                    </div>
                    <div class="col-md-10">
                        <div class="menu">
                            <ul>
                                <li class="active">
                                    <?php
                                    echo Nav::widget([
                                        'items' => [
                                            ['label' => 'Home', 'url' => ['/post/index']]
                                        ]
                                    ]);
                                    ?>
                                </li>
                                <li>
                                    <?php
                                    if (!Yii::$app->user->isGuest){
                                        echo Nav::widget([
                                            'items' => [
                                                ['label' => 'Post', 'url' => ['/post/post']]
                                            ]
                                        ]);
                                    }
                                    ?>
                                </li>
                                <li>
                                    <?php
                                    echo Nav::widget([
                                        'items' => [
                                            ['label' => 'Register', 'url' => ['/site/register'], 'visible' => Yii::$app->user->isGuest]
                                        ]
                                    ]);
                                    ?>
                                </li>
                                <li>
                                    <?php
                                    echo Nav::widget([
                                        'items' => [
                                            Yii::$app->user->isGuest
                                                ? ['label' => 'Login', 'url' => ['/site/login']]
//                                                : ['label' => 'Logout', 'url' => ['/s ite/logout']]
                                                : '<li class="nav-item">'
                                                . Html::beginForm(['/site/logout'])
                                                . Html::submitButton(
                                                    'Logout (' . Yii::$app->user->identity->username . ')',
                                                    ['class' => 'nav-link btn btn-link logout']
                                                )
                                                . Html::endForm()
                                                . '</li>'
                                        ]
                                    ]);
                                    ?>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </header>

        <!--        --><?php //= $content ?>

        <main id="main" class="flex-shrink-0" role="main">
            <div class="container">
                <?php if (!empty($this->params['breadcrumbs'])): ?>
                    <?= Breadcrumbs::widget(['links' => $this->params['breadcrumbs']]) ?>
                <?php endif ?>
                <?= Alert::widget() ?>
                <?= $content ?>
            </div>
        </main>

        <footer class="footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="footer-bg">
                            <div class="row">
                                <div class="col-md-9">
                                    <div class="footer-menu">
                                        <ul>
                                            <li class="active"><a href="#">Home</a></li>
                                            <li><a href="#">Register</a></li>
                                            <li><a href="#">Login</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="footer-icon">
                                        <p><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a><a href="#"><i
                                                        class="fa fa-twitter" aria-hidden="true"></i></a><a href="#"><i
                                                        class="fa fa-linkedin" aria-hidden="true"></i></a><a href="#"><i
                                                        class="fa fa-dribbble" aria-hidden="true"></i></a></p>
                                    </div>
                                </div>
                            </div>
                            .
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>
    <?php $this->endBody() ?>
    </body>

    </html>
<?php $this->endPage() ?>