<?php

/** @var yii\web\View $this */
/** @var yii\bootstrap5\ActiveForm $form */

/** @var app\models\RegisterForm $model */

use yii\bootstrap5\ActiveForm;
use yii\bootstrap5\Html;

$this->title = 'Register';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>Please fill out the following fields to register:</p>

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin([
                'id' => 'login-form',
                'fieldConfig' => [
                    'template' => "{label}\n{input}\n{error}",
                    'labelOptions' => ['class' => 'col-lg-1 col-form-label mr-lg-3'],
                    'inputOptions' => ['class' => 'col-lg-3 form-control'],
                    'errorOptions' => ['class' => 'col-lg-7 invalid-feedback'],
                ],
            ]); ?>

            <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>
            <?= $form->field($model, 'first_name') ?>
            <?= $form->field($model, 'last_name') ?>
            <?= $form->field($model, 'email') ?>

            <?= $form->field($model, 'password')->passwordInput() ?>

            <div class="form-group">
                <div class="offset-lg-1 col-lg-11">
                    <?= Html::submitButton('Register', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                </div>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

<!--<section class="vh-100" style="background-color: #ffffff;">-->
<!--    <div class="container h-100">-->
<!--        <div class="row d-flex justify-content-center align-items-center h-100">-->
<!--            <div class="col-lg-12 col-xl-11">-->
<!--                <div class="card text-black" style="border-radius: 25px;">-->
<!--                    <div class="card-body p-md-5">-->
<!--                        <div class="row justify-content-center">-->
<!--                            <div class="col-md-10 col-lg-6 col-xl-5 order-2 order-lg-1">-->
<!---->
<!--                                <p class="text-center h1 fw-bold mb-5 mx-1 mx-md-4 mt-4">Sign up</p>-->
<!---->
<!--                                <form class="mx-1 mx-md-4">-->
<!---->
<!--                                    <div class="d-flex flex-row align-items-center mb-4">-->
<!--                                        <i class="fas fa-user fa-lg me-3 fa-fw"></i>-->
<!--                                        <div class="form-outline flex-fill mb-0">-->
<!--                                            <input type="text" id="form3Example1c" class="form-control" />-->
<!--                                            <label class="form-label" for="form3Example1c">Username</label>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!---->
<!--                                    <div class="d-flex flex-row align-items-center mb-4">-->
<!--                                        <i class="fas fa-user fa-lg me-3 fa-fw"></i>-->
<!--                                        <div class="form-outline flex-fill mb-0">-->
<!--                                            <input type="text" id="form3Example1c" class="form-control" />-->
<!--                                            <label class="form-label" for="form3Example1c">First Name</label>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!---->
<!--                                    <div class="d-flex flex-row align-items-center mb-4">-->
<!--                                        <i class="fas fa-user fa-lg me-3 fa-fw"></i>-->
<!--                                        <div class="form-outline flex-fill mb-0">-->
<!--                                            <input type="text" id="form3Example1c" class="form-control" />-->
<!--                                            <label class="form-label" for="form3Example1c">Last Name</label>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!---->
<!--                                    <div class="d-flex flex-row align-items-center mb-4">-->
<!--                                        <i class="fas fa-envelope fa-lg me-3 fa-fw"></i>-->
<!--                                        <div class="form-outline flex-fill mb-0">-->
<!--                                            <input type="email" id="form3Example3c" class="form-control" />-->
<!--                                            <label class="form-label" for="form3Example3c">Email</label>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!---->
<!--                                    <div class="d-flex flex-row align-items-center mb-4">-->
<!--                                        <i class="fas fa-lock fa-lg me-3 fa-fw"></i>-->
<!--                                        <div class="form-outline flex-fill mb-0">-->
<!--                                            <input type="password" id="form3Example4c" class="form-control" />-->
<!--                                            <label class="form-label" for="form3Example4c">Password</label>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!---->
<!--                                    <div class="form-check d-flex justify-content-center mb-5">-->
<!--                                        <input class="form-check-input me-2" type="checkbox" value="" id="form2Example3c" />-->
<!--                                        <label class="form-check-label" for="form2Example3">-->
<!--                                            I agree all statements in <a href="#!">Terms of service</a>-->
<!--                                        </label>-->
<!--                                    </div>-->
<!---->
<!--                                    <div class="d-flex justify-content-center mx-4 mb-3 mb-lg-4">-->
<!--                                        <button type="button" class="btn btn-primary btn-lg">Register</button>-->
<!--                                    </div>-->
<!---->
<!--                                </form>-->
<!---->
<!--                            </div>-->
<!--                            <div class="col-md-10 col-lg-6 col-xl-7 d-flex align-items-center order-1 order-lg-2">-->
<!---->
<!--                                <img src="https://mdbcdn.b-cdn.net/img/Photos/new-templates/bootstrap-registration/draw1.webp"-->
<!--                                     class="img-fluid" alt="Sample image">-->
<!---->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</section>-->
